export const getGif = async (category) => {
    const url = `https://api.giphy.com/v1/gifs/search?q=${encodeURI(category)}&limit=12&api_key=OvoGMvlEbHQTqCvwVMRYjyu7S7qYzDp2`;
    try {
        const resp = await fetch(url);
        const { data } = await resp.json();
        console.log(data)
        const gifs = data.map(({ id, title, images,bitly_url }) => {
            return { 
                id,
                title,
                url: images.downsized_medium.url,
                bitly_url
            }
        
        });
        
        return (gifs);

    } catch (error) {
        console.log(error);

    }
}