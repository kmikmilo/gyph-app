import React from 'react'

export const GifGridItem = (({ title, url,bitly_url }) => {

    return (
        <div className="card animate__animated animate__flipInX">
            <img src={url} alt={title} />
            <p>{title}</p>
            <p><a href={bitly_url}>link</a></p>
            
        </div>
    )
})
