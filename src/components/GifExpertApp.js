import { useState } from "react";
import { CategoryAdd } from "./CategoryAdd";
import { GifGrid } from "./GifGrid";

export const GifExpertApp = () => {

    // const categorias = ['One Punc', 'jujutsu kaisen', 'DBZ'];
    const [category, setCategory] = useState(['One Punch']);
    /*  const HandleAdd = () => {
         // setCategory([...category,'Hunter x Hunter']);
         setCategory(category =>[...category,'HunterXHunter']); 
     }*/
    console.log(category);
    return (
        <> {/* fragment */}
            <div className="cabecera">
                <header> <h2>GifExpertApp 1.0</h2></header>
                
            </div>

            <p><label className="buscar">Buscar</label></p>
            <CategoryAdd
                setCategory={setCategory}
                category={category}
            />
            <hr></hr>
            <ol>
                {
                    category.map((anime) => {
                        return <GifGrid
                            key={anime}
                            category={anime} />
                    })
                }
            </ol>
        </>
    );
}

