import React, { useState } from 'react';
import PropTypes from 'prop-types';

export const CategoryAdd = ({ setCategory }) => {

    const [inputValue, setinputValue] = useState('');

    //obteniendo el value del input
    const handleInputChange = (e) => {

        setinputValue(e.target.value)
    }

    //definiendo que hace el submit
    const handleSubmit = (e) => {
        e.preventDefault();

        if (inputValue.trim().length > 2) {
            setCategory(anime => [inputValue, ...anime]);
            setinputValue('');
        }
        // category.some(anime =>anime === inputValue ? console.log('igual'): console.log('no igual')  )
    }
    return (

        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                autoFocus
            />
        </form>

    )
}

CategoryAdd.propTypes = {
    setCategory: PropTypes.func.isRequired
}
