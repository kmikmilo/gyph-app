import { useFetchGifs } from '../hooks/useFetchGifs';
import { GifGridItem } from './GifGridItem';

export const GifGrid = ({ category }) => {

    const { data: images, loading } = useFetchGifs(category);

    return (
        <>
            <h3 className='animate__animated animate__flipInX'>{category}</h3>
            {loading ? <p className="animate__animated animate__flash">Laoding</p> : <p>Ready</p>}
            <div className="card-grid">

                {
                    images.map(img =>
                    (
                        <GifGridItem
                            key={img.id}
                            // img={img} esto es lo mismo que la linea de abajo
                            {...img}
                        />
                    ))
                }
            </div>
        </>
    )
}
